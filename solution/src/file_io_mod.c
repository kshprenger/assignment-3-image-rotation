#include "../include/file_io/file_open.h"
#include "../include/file_io/file_close.h"
#include <stddef.h>
enum open_file_status open_file( FILE** file, const char* pathname, const char*  mode ){
    *file = fopen(pathname, mode);
    return *file==NULL ? OPEN_FAIL : OPEN_SUCCESS;
}

enum close_file_status close_file( FILE* file){
    return fclose(file)==0 ? CLOSE_SUCCESS : CLOSE_FAIL;
}

