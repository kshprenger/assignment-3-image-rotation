#include "../include/rotation.h"
#include "../include/image_struct.h"
#include <string.h>
struct image rotate( const struct image source ){
    struct image image = create_new_image(source.height,source.width); //Creating blank rotated image

    if(image.data==NULL){ //Can't provide memoty - return image with NULL data
        return image;
    }

    for (size_t i = 0; i < source.height; i++)
    {
        for (size_t j = 0; j < source.width; j++)
        {
            image.data[j*image.width + i] = source.data[(source.height - i - 1)*source.width + j];
        }
        
    }
    return image;
}
