#include "../include/bmp/bmp_format.h"
#include "../include/bmp/bmp_header_struct.h"
#include "../include/image_struct.h"

static uint32_t padding_calculation(const uint32_t width){
    uint32_t padding = (4 - (width*3)%4)%4;
    return padding;
}

static uint32_t image_size_calculation(const struct image* img){
    return (img->height) * (sizeof(struct pixel) * (img->width) + padding_calculation(img->width)); //Each row has padding
}

struct bmp_header generate_bmp_header(const struct image* img){
    uint32_t image_size = image_size_calculation(img);

   return (struct bmp_header){
        .bfType = BMP_SIGNATURE_LE,
        .bfileSize = sizeof(struct bmp_header) + image_size,
        .bfReserved = 0, //Must be zero
        .bOffBits = BMP_HEADER_TOTAL_BYTE_SIZE,
        .biSize = BITMAPINFOHEADER_TOTAL_BYTE_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1, //Only 1 is available
        .biBitCount = PIXEL_TOTAL_BIT_SIZE,
        .biCompression = RGB_COMPRESSION,
        .biSizeImage = image_size,
        .biXPelsPerMeter = UNUSED,
        .biYPelsPerMeter = UNUSED,
        .biClrUsed = UNUSED,
        .biClrImportant = UNUSED
    };
}

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header;
    const size_t count = fread(&header, sizeof(struct bmp_header), 1, in);

    if(header.bfType!=BMP_SIGNATURE_LE && header.bfType!=BMP_SIGNATURE_BE){
        return READ_INVALID_SIGNATURE;
    }

    if(header.biBitCount!=PIXEL_TOTAL_BIT_SIZE){
        return READ_INVALID_BITS;
    }

    if(count==0){
        return READ_INVALID_HEADER;
    }

    if(header.biSize==0){
        return READ_INVALID_FILE_SIZE;
    }

    if(header.biWidth==0||header.biHeight==0){
        return READ_INVALID_IMAGE_SIZE;
    }

    *img = create_new_image(header.biWidth, header.biHeight);

    if(img->data==NULL){ //Can't provide memory for image
        return READ_ERROR;
    }
    uint64_t padding = padding_calculation(header.biWidth);
    for (size_t i = 0; i < header.biHeight; i++)
    {  
        if(fread((img->data) + i*header.biWidth,sizeof(struct pixel) * header.biWidth, 1, in)==0){ //Reading pixels by rows; If something is going wrong -> deleting image
            delete_image(*img);
            return READ_ERROR;
        }
        if(fseek(in,(int64_t)padding,SEEK_CUR)==-1){ //Skipping garbage bytes
            delete_image(*img);
            return READ_ERROR;
        }
    }
    
    return READ_OK; 
}

enum write_status to_bmp( FILE* out, const struct image* img ){  
    const uint32_t padding = padding_calculation(img->width);
    const uint32_t padding_bytes = 0; //Garbage bytes
    struct bmp_header header = generate_bmp_header(img); 

    if(fwrite(&header, sizeof(struct bmp_header),1,out)==0){
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < img->height; i++)
    {
        if(fwrite(img->data + i*img->width, sizeof(struct pixel)*img->width,1,out ) == 0){ //Writing pure image by rows
            delete_image(*img);
            return WRITE_ERROR;
        }
        if(padding!=0 && (fwrite(&padding_bytes, padding,1,out ) == 0)){     //Writing garbage bytes; Padding may be equal 0
            delete_image(*img);             
            return WRITE_ERROR;
        }
    }

    return WRITE_OK; 
}
