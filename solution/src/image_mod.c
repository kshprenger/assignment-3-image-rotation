#include "../include/image_struct.h"
#include <stddef.h>
#include <stdlib.h>
struct image create_new_image(const uint64_t width, const uint64_t height){
    struct pixel* data = malloc(sizeof(struct pixel) * (width * height));
    return (struct image){.width = width, .height = height, .data = data};
}

void delete_image(struct image image){
    free(image.data);
    image.height = 0;
    image.width = 0;
}
