#include "../include/main.h"
#include <stdio.h>
#include <stdlib.h>
int main( int argc, char** argv ) {
    
    struct image image;
    struct image rotated_image;
    enum open_file_status open_file_status;
    enum close_file_status close_file_status;
    enum read_status read_status;
    enum write_status write_status;

    if(argc != 3){
        printf(ARG_NUMBER_ERROR);
        return 1;
    }

    FILE** file_in = malloc(sizeof(FILE*));
    open_file_status = open_file(file_in, argv[1],"r");
    if(open_file_status == OPEN_FAIL){
        printf(FILE_OPEN_ERROR);
        free(file_in);
        return 1;
    }

    FILE** file_out = malloc(sizeof(FILE*));
    open_file_status = open_file(file_out, argv[2],"w");
    if(open_file_status == OPEN_FAIL){
        printf(FILE_OPEN_ERROR);
        free(file_in);
        free(file_out);
        return 1;
    }

    read_status = from_bmp(*file_in,&image);
    switch (read_status)
    {
    case READ_ERROR:
        printf(BMP_READ_ERROR);
        break;
    case READ_INVALID_SIGNATURE:
        printf(BMP_INVALID_SIGNATURE_ERROR);
        break;
    case READ_INVALID_BITS:
        printf(BMP_READ_INVALID_BITS_ERROR);
        break;
    case READ_INVALID_FILE_SIZE:
        printf(BMP_READ_INVALID_FILE_SIZE_ERROR);
        break;
    case READ_INVALID_IMAGE_SIZE:
        printf(BMP_READ_INVALID_IMAGE_SIZE_ERROR);
        break;
    case READ_INVALID_HEADER:
        printf(BMP_READ_INVALID_HEADER_ERROR);
        break;
    default:
        break;
    }
    if(read_status!=READ_OK){
        free(file_in);
        free(file_out);
        return 1;
    }

    rotated_image = rotate(image);
    if(rotated_image.data==NULL){
        printf(MEMORY_FOR_IMAGE_ERROR);
        delete_image(image);
        free(file_in);
        free(file_out);
        return 1;
    }

    write_status = to_bmp(*file_out, &rotated_image);
    if(write_status==WRITE_ERROR){
        delete_image(image);
        delete_image(rotated_image);
        free(file_in);
        free(file_out);
        printf(BMP_WRITE_ERROR);
        return 1;
    }

    close_file_status = close_file(*file_in);
    if(close_file_status == CLOSE_FAIL){
        printf(FILE_CLOSE_ERROR);
        delete_image(image);
        delete_image(rotated_image);
        free(file_in);
        free(file_out);
        return 1;
    }

    close_file_status = close_file(*file_out);
    if(close_file_status == CLOSE_FAIL){
        printf(FILE_CLOSE_ERROR);
        delete_image(image);
        delete_image(rotated_image);
        free(file_in);
        free(file_out);
        return 1;
    }

    delete_image(image);
    delete_image(rotated_image);
    free(file_in);
    free(file_out);
    return 0;
}
