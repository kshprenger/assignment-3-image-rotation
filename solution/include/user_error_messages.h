//A bunch of messages for users, to understand what's going on

#define ARG_NUMBER_ERROR "Please use this format: \"./image-transformer <source-image> <transformed-image>\"\n"
#define FILE_OPEN_ERROR "Can't open file\n"
#define FILE_CLOSE_ERROR "Can't close file\n"
#define BMP_READ_ERROR "Can't read or provide memory for bmp file\n"
#define BMP_WRITE_ERROR "Can't write bmp file\n"
#define BMP_INVALID_SIGNATURE_ERROR "Wrong bmp file signature\n"
#define BMP_READ_INVALID_BITS_ERROR "Invalid bits number per pixel\n"
#define BMP_READ_INVALID_HEADER_ERROR "Can't read bmp file header\n"
#define BMP_READ_INVALID_FILE_SIZE_ERROR "Invalid file size\n"
#define BMP_READ_INVALID_IMAGE_SIZE_ERROR "Invalid image size\n"
#define MEMORY_FOR_IMAGE_ERROR "Can't provide memory for new image\n"
