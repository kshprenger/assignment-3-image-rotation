#ifndef BMP_FORMAT_HEADER
#define BMP_FORMAT_HEADER
#include "../image_struct.h"
#include <stdio.h>
enum read_status  {//Bunch of errors for reading
  READ_OK = 0,
  READ_ERROR,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_FILE_SIZE,
  READ_INVALID_IMAGE_SIZE
};


enum read_status from_bmp(FILE * in, struct image* img );

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum write_status to_bmp(FILE * out, const struct image * img );


#endif
