#ifndef BMP_HEADER_HEADER
#define BMP_HEADER_HEADER
#include <stdint.h>
#define BMP_SIGNATURE_LE 0x4D42                 //Constants for bmp header      
#define BMP_SIGNATURE_BE 0x424D
#define PIXEL_TOTAL_BIT_SIZE 24
#define BMP_HEADER_TOTAL_BYTE_SIZE 54
#define BITMAPINFOHEADER_TOTAL_BYTE_SIZE 40
#define RGB_COMPRESSION 0
#define UNUSED 0
struct __attribute__((packed)) bmp_header 
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};

struct bmp_header generate_bmp_header(const struct image* img);

#endif
