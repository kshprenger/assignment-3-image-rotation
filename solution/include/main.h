#ifndef MAIN_HEADER
#define MAIN_HEADER
#include "bmp/bmp_format.h"
#include "bmp/bmp_header_struct.h"
#include "file_io/file_close.h" //There is no file_in and file_out special modules,
#include "file_io/file_open.h"  //because bmp_format.h already has similar errors and logic
#include "image_struct.h"       //But for wider architecture, surely, it's needed
#include "rotation.h"
#include "user_error_messages.h"
#endif

