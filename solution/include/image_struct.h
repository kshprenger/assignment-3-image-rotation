#ifndef IMAGE_STRUCT_HEADER
#define IMAGE_STRUCT_HEADER
#include <stdint.h>

struct __attribute__((packed)) pixel 
{
  uint8_t b, g, r; 
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image create_new_image(const uint64_t width, const uint64_t height); 

void delete_image(struct image image);                                     

#endif
