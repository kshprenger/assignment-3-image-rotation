#ifndef FILE_CLOSE_HEADER
#define FILE_CLOSE_HEADER
#include <stdio.h>

enum  close_file_status  {
  CLOSE_SUCCESS = 0,
  CLOSE_FAIL
};

enum close_file_status close_file(FILE* file);

#endif

