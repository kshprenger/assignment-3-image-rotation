#ifndef ROTATION_HEADER
#define ROTATION_HEADER
#include "image_struct.h"

struct image rotate( const struct image source );

#endif
